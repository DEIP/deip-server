import UserInviteService from './../../services/legacy/userInvites';


const getUserInvites = async (ctx) => {
  const jwtUsername = ctx.state.user.username;
  const username = ctx.params.username;
  const userInviteService = new UserInviteService();

  try {

    if (jwtUsername != username) {
      ctx.status = 401;
      ctx.body = `"${jwtUsername}" is not permitted to view invites for "${username}" research`;
      return;
    }

    const activeInvites = await userInviteService.findUserPendingInvites(username);
    ctx.status = 200;
    ctx.body = activeInvites;
    
  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = err;
  }
}


const getResearchGroupPendingInvites = async (ctx) => {
  const jwtUsername = ctx.state.user.username;
  const researchGroupExternalId = ctx.params.researchGroupExternalId;
  const userInviteService = new UserInviteService();

  try {

    const invites = await userInviteService.findResearchGroupPendingInvites(researchGroupExternalId);
    ctx.status = 200;
    ctx.body = invites;

  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = err;
  }
}


const getResearchPendingInvites = async (ctx) => {
  const jwtUsername = ctx.state.user.username;
  const researchExternalId = ctx.params.researchExternalId;
  const userInviteService = new UserInviteService();

  try {

    const invites = await userInviteService.findResearchPendingInvites(researchExternalId);
    ctx.status = 200;
    ctx.body = invites;

  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = err;
  }
}


const createUserInvite = async (ctx, next) => {
  // DEPRECATED: Use APP_PROPOSAL.PROJECT_INVITE_PROPOSAL instead

  try {

    ctx.status = 200;
    ctx.body = "DEPRECATED: Use APP_PROPOSAL.PROJECT_INVITE_PROPOSAL flow instead";

  } catch (err) {
    console.log(err);
    ctx.status = 500;
    ctx.body = err;
  }

  await next();

};


export default {
  getUserInvites,
  getResearchPendingInvites,
  getResearchGroupPendingInvites,
  createUserInvite
}