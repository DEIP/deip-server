import projectsCtrl from '../controllers/impl/ProjectsController';
import proposalsCtrl from '../controllers/impl/ProposalsController';


module.exports = {
  projectsCtrl,
  proposalsCtrl
}