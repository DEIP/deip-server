import PubSub from 'pubsub-js';
import BaseEventHandler from './../event-handlers/base/BaseEventHandler';
import { QUEUE_TOPIC } from './../constants';


PubSub.subscribe(QUEUE_TOPIC.APP_EVENT_TOPIC, async function (topic, events) {
  // TODO: we should use FIFO queue with an acknowledgment mechanism instead
  await BaseEventHandler.Broadcast(events);
});