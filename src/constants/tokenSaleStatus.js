const ACTIVE = 1;
const FINISHED = 2;
const EXPIRED = 3;
const INACTIVE = 4;

const TOKEN_SALE_STATUS = {
  ACTIVE,
  FINISHED,
  EXPIRED,
  INACTIVE
}

export default TOKEN_SALE_STATUS;