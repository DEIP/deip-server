const FREE = "free";
const ADMIN_APPROVAL = "admin-approval";

const NEW_RESEARCH_POLICY = {
  FREE,
  ADMIN_APPROVAL
};

export default NEW_RESEARCH_POLICY;