const PROPOSED = "proposed";
const SENT = "sent";
const APPROVED = "approved";
const REJECTED = "rejected";
const EXPIRED = "expired";

const USER_INVITE_STATUS = {
  PROPOSED, // deprecated
  SENT,
  APPROVED,
  REJECTED,
  EXPIRED
}

export default USER_INVITE_STATUS;


