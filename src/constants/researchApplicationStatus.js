const PENDING = "pending";
const APPROVED = "approved";
const REJECTED = "rejected";
const DELETED = "deleted";

const RESEARCH_APPLICATION_STATUS = {
  PENDING,
  APPROVED,
  REJECTED,
  DELETED
}

export default RESEARCH_APPLICATION_STATUS;