const PROPOSAL = "proposal";
const PROPOSAL_ACCEPTED = "proposal-accepted";
const INVITATION = "invitation";
const INVITATION_APPROVED = "invitation-approved";
const INVITATION_REJECTED = "invitation-rejected";
const EXCLUSION_APPROVED = "exclusion-approved";
const RESEARCH_CONTENT_EXPERT_REVIEW = "research-content-expert-review";
const RESEARCH_CONTENT_EXPERT_REVIEW_REQUEST = "research-content-expert-review-request";
const RESEARCH_APPLICATION_CREATED = "research-application-created";
const RESEARCH_APPLICATION_APPROVED = "research-application-approved";
const RESEARCH_APPLICATION_REJECTED = "research-application-rejected";
const RESEARCH_APPLICATION_EDITED = "research-application-edited";
const RESEARCH_APPLICATION_DELETED = "research-application-deleted";
const RESEARCH_NDA_PROPOSED = "research-nda-request";
const RESEARCH_NDA_SIGNED = "research-nda-signed";
const RESEARCH_NDA_REJECTED = "research-nda-rejected";


const USER_NOTIFICATION_TYPE = {
  PROPOSAL,
  PROPOSAL_ACCEPTED,
  INVITATION,
  INVITATION_APPROVED,
  INVITATION_REJECTED,
  EXCLUSION_APPROVED,
  RESEARCH_CONTENT_EXPERT_REVIEW,
  RESEARCH_CONTENT_EXPERT_REVIEW_REQUEST,
  RESEARCH_APPLICATION_CREATED,
  RESEARCH_APPLICATION_APPROVED,
  RESEARCH_APPLICATION_REJECTED,
  RESEARCH_APPLICATION_EDITED,
  RESEARCH_APPLICATION_DELETED,
  RESEARCH_NDA_PROPOSED,
  RESEARCH_NDA_SIGNED,
  RESEARCH_NDA_REJECTED
}

export default USER_NOTIFICATION_TYPE;