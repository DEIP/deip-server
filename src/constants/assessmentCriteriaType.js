const UNKNOWN = 0;
const NOVELTY = 1;
const TECHNICAL_QUALITY = 2;
const METHODOLOGY = 3;
const IMPACT = 4;
const RATIONALITY = 5;
const REPLICATION = 6;
const COMMERCIALIZATION = 7;

const ASSESSMENT_CRITERIA_TYPE = {
  UNKNOWN,
  NOVELTY,
  TECHNICAL_QUALITY,
  METHODOLOGY,
  IMPACT,
  RATIONALITY,
  REPLICATION,
  COMMERCIALIZATION
};

export default ASSESSMENT_CRITERIA_TYPE;