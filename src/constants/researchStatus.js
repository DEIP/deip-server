const PROPOSED = "proposed";
const APPROVED = "approved";
const DELETED = "deleted";

const RESEARCH_STATUS = {
  PROPOSED,
  APPROVED,
  DELETED
}

export default RESEARCH_STATUS;


