

const IN_PROGRESS = "in-progress";
const PROPOSED = "proposed";
const PUBLISHED = "published";


const RESEARCH_CONTENT_STATUS = {
  IN_PROGRESS,
  PROPOSED,
  PUBLISHED
}

export default RESEARCH_CONTENT_STATUS;