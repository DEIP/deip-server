const PENDING = "pending";
const APPROVED = "approved";

const USER_PROFILE_STATUS = {
  PENDING,
  APPROVED
}

export default USER_PROFILE_STATUS;