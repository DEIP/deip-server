const FREE = "free";
const ADMIN_APPROVAL = "admin-approval";

const SIGN_UP_POLICY = {
  FREE,
  ADMIN_APPROVAL
};

export default SIGN_UP_POLICY;