import assert from 'assert';
import { LEGACY_APP_EVENTS } from './../../constants';
import AppEvent from './appEvent';
import ProposalEvent from './proposalEvent';

class UserResignationProposedEvent extends ProposalEvent(AppEvent) {
  constructor(onchainDatums, offchainMeta, eventName = LEGACY_APP_EVENTS.USER_RESIGNATION_PROPOSED) {
    assert(onchainDatums.some(([opName]) => opName == 'leave_research_group_membership'), "leave_research_group_membership_operation is not provided");
    super(onchainDatums, offchainMeta, eventName);
  }

  getSourceData() {
    const [opName, opPayload] = this.onchainDatums.find(([opName]) => opName == 'leave_research_group_membership');
    const { member, research_group: researchGroupExternalId } = opPayload;
    const { notes } = this.offchainMeta;
    return { ...super.getSourceData(), member, researchGroupExternalId, notes };
  }
}

module.exports = UserResignationProposedEvent;